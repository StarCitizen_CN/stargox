set CMD_NAME=fixme
set BIN_NAME=fixme.exe
call build.bat %CMD_NAME% %BIN_NAME%

set OUTPUT_PATH=%BUILD_DIR%\out\%BIN_NAME%

del %OUTPUT_PATH%

echo %OUTPUT_PATH%
go build -ldflags "-s -w -H=windowsgui" -o %OUTPUT_PATH% .\cmd\%CMD_NAME%\.
@REM go build -o %OUTPUT_PATH% .\cmd\%CMD_NAME%\.

%UPX_PATH% %OUTPUT_PATH%
Start "" %BUILD_DIR%\out