package main

import (
	"errors"
	"flag"
	"fmt"
	"path/filepath"

	"gitee.com/StarCitizen_CN/stargox/internal/app/sgbs"
	"gitee.com/StarCitizen_CN/stargox/pkg/fstools"
	"gitee.com/StarCitizen_CN/stargox/pkg/logger"
)

var (
	workPort = flag.Int("p", 9567, "Listening port.")
	isDebug  = flag.Bool("d", true, "Enable debug mode.")
)

// todo 成为 Windows Service 参考 https://github.com/kardianos/service
func main() {

	flag.Parse()

	// 必须最先初始化 懂的都懂
	logger.Init(filepath.Join(fstools.GetExeDirPath(), "log\\sgbs"), true, false)

	// 捞屎罢了
	defer func() {
		logger.Debug("StarGox Shutdown")

		if r := recover(); r != nil {
			logger.Err(errors.New(fmt.Sprintln(r)))
		}
	}()

	// 上菜 time
	sgbs.InitService(*workPort)
}
