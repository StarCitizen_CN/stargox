package checker

const targetPart0 = "Bin64\\CIGDevelopmentTools.dll"
const targetPart1 = "data\\"

type L10nScanReport struct {
	Name    string     `json:"l10n_name"`
	PakAll  []L10nFile `json:"pak_all"`
	PakPart []L10nFile `json:"pak_part"`

	PakFullMd5 string `json:"pak_all_md5"`
	PakPartMd5 string `json:"pak_part_md5"`
}

type L10nFile struct {
	Name string `json:"name"`
	Path string `json:"path"`
	Md5  string `json:"md5"`
}

type L10nFileData struct {
	L10nFile
}
