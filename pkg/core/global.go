package core

import (
	"os"
	"path/filepath"
)

var (
	// 用于检测更新
	GitVersion = ""

	TempDir = filepath.Join(os.TempDir(), "starbox")
)

func Destroy() {
	TempDir = ""
	GitVersion = ""
}
