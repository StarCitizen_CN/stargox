package fstools

var (
	L10nDownloadDir string
	ContentDataDir  string
	TEMPDir         string
)

type HashAndCopyWorker func(src string) (hashMap map[string]string, err error)

type HashAndCopyResult struct {
	md5Map map[string]string
	err    error
}
