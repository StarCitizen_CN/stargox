@echo on

@REM 如何使用： call build.bat CMD_NAME BIN_NAME

set CMD_NAME=%~1
set BIN_NAME=%~2

@REM 构建所需目录
set BUILD_DIR=build\%CMD_NAME%
IF NOT EXIST %BUILD_DIR% MD %BUILD_DIR%

set TARGET=%BUILD_DIR%\%BIN_NAME%

@REM TODO 构建环境配置脚本
set BUILD_CIG=build\build_config.bat
IF NOT EXIST %BUILD_CIG% (
    echo set UPX_PATH=D:\"Program Files"\upx\upx.exe > %BUILD_CIG% 
    echo set GCC_PATH=D:\Development\Env\msys64\mingw64\bin >> %BUILD_CIG%
)
@REM 执行环境脚本
call %BUILD_CIG%

set PATH=%PATH%;%UPX_PATH%
set PATH=%PATH%;%GCC_PATH%

del %TARGET%

