package boxcfg

const (
	CfgRoamingPath = "starbox\\boxcfg.json"
)

// 需要与 flutter 端保持一致
// 用于安装程序读取
type AppProperty struct {
	Version       string `json:"app_version"`
	InstalledPath string `json:"installed_path"`
}

type PatchMeta struct {
	BoxVersion  string `json:"box_version"`
	L10nVersion string `json:"l10n_version"`
	PatchCode   string `json:"patch_code"`
}

type L10nMeta struct {
	Version   string `json:"version"`
	Content   string `json:"content"`
	PatchCode string `json:"patch_code"`
}
