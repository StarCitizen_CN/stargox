package boxcfg

import (
	"encoding/json"
	"os"
)

func ReadCfg(cfgPath string) (property *AppProperty, err error) {

	_, cfgCheckErr := os.Stat(cfgPath)
	if os.IsNotExist(cfgCheckErr) {
		return nil, nil
	}
	if cfgCheckErr != nil {
		return nil, cfgCheckErr
	}

	cfgBytes, readShareJsonErr := os.ReadFile(cfgPath)
	if readShareJsonErr != nil {
		return nil, readShareJsonErr
	}

	property = &AppProperty{}

	jsonErr := json.Unmarshal(cfgBytes, property)
	if jsonErr != nil {
		return nil, jsonErr
	}

	return
}

func WriteCfg(property *AppProperty) (err error) {
	// 暂时无得写，咕咕咕
	return
}

func ReadL10nMeta(location string) (meta *L10nMeta, err error) {
	data, readErr := os.ReadFile(location)
	if readErr != nil {
		return nil, readErr
	}

	m := &L10nMeta{}
	if unMarshalErr := json.Unmarshal(data, m); unMarshalErr != nil {
		return nil, unMarshalErr
	}
	meta = m

	return meta, nil
}
