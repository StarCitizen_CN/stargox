package gitee

import (
	"path"
	"strings"
)

type GiteeTag struct {
	Name    string      `json:"name"`
	Message string      `json:"message"`
	Commit  GiteeCommit `json:"commit"`
}

type GiteeCommit struct {
	Sha  string `json:"sha"`
	Date string `json:"date"`
}

type Release struct {
	TagName        string           `json:"tag_name"`
	IsPreRelease   bool             `json:"prerelease"`
	VersionName    string           `json:"name"`
	VersionDetails string           `json:"body"`
	Assets         []*ReleaseAssets `json:"assets"`
}

type ReleaseAssets struct {
	// 资源名字，如果没有就别使用。
	Name string `json:"name"`
	Url  string `json:"browser_download_url"`
}

func (asset *ReleaseAssets) GetDownloadUrl() string {

	if asset == nil {
		return ""
	}

	// gitee 会变动
	if asset.Name != "" && !strings.Contains(asset.Url, asset.Name) {
		return path.Join(asset.Url, asset.Name)
	}

	return asset.Url
}
