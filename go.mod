module gitee.com/StarCitizen_CN/stargox

go 1.19

require (
	fyne.io/fyne/v2 v2.2.3
	gitee.com/StarCitizen_CN/logger v0.0.0-20221029202530-92f3b154c99f
	gitee.com/klkrep/ging v0.0.0-20220621205332-7d8b8b532737
	gitee.com/klkrep/starboxapi v0.0.0
	github.com/atotto/clipboard v0.1.4
	github.com/go-git/go-git/v5 v5.4.2
	github.com/otiai10/copy v1.7.0
	github.com/shirou/gopsutil/v3 v3.22.9
	golang.org/x/net v0.1.0
	golang.org/x/sys v0.1.0
	google.golang.org/grpc v1.50.1
	google.golang.org/protobuf v1.28.1
	gopkg.in/yaml.v3 v3.0.1
)

// 仅供开发使用
replace gitee.com/klkrep/starboxapi v0.0.0 => ../starboxapi

require (
	fyne.io/systray v1.10.1-0.20220621085403-9a2652634e93 // indirect
	github.com/Microsoft/go-winio v0.6.0 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20221026131551-cf6655e29de4 // indirect
	github.com/acomagu/bufpipe v1.0.3 // indirect
	github.com/cloudflare/circl v1.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/fredbi/uri v0.0.0-20221012073901-fb871453c6d3 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/fyne-io/gl-js v0.0.0-20220802150000-8e339395f381 // indirect
	github.com/fyne-io/glfw-js v0.0.0-20220517201726-bebc2019cd33 // indirect
	github.com/fyne-io/image v0.0.0-20221020213044-f609c6a24345 // indirect
	github.com/go-git/gcfg v1.5.0 // indirect
	github.com/go-git/go-billy/v5 v5.3.1 // indirect
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20221017161538-93cebf72946b // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/goki/freetype v0.0.0-20220119013949-7a161fd3728c // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gopherjs/gopherjs v1.17.2 // indirect
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/jsummers/gobmp v0.0.0-20151104160322-e2ba15ffa76e // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/lufia/plan9stats v0.0.0-20220913051719-115f729f3c8c // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/power-devops/perfstat v0.0.0-20220216144756-c35f1ee13d7c // indirect
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/srwiley/oksvg v0.0.0-20221011165216-be6e8873101c // indirect
	github.com/srwiley/rasterx v0.0.0-20220730225603-2ab79fcdd4ef // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	github.com/tevino/abool v1.2.0 // indirect
	github.com/xanzy/ssh-agent v0.3.2 // indirect
	github.com/yuin/goldmark v1.5.2 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/crypto v0.1.0 // indirect
	golang.org/x/image v0.1.0 // indirect
	golang.org/x/mobile v0.0.0-20221020085226-b36e6246172e // indirect
	golang.org/x/mod v0.6.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	golang.org/x/tools v0.2.0 // indirect
	google.golang.org/genproto v0.0.0-20221027153422-115e99e71e1c // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
	honnef.co/go/js/dom v0.0.0-20221001195520-26252dedbe70 // indirect
)
