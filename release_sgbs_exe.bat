set CMD_NAME=sgbs
set BIN_NAME=sgbs.exe
call build.bat %CMD_NAME% %BIN_NAME%

set OUTPUT_PATH=%BUILD_DIR%\out\%BIN_NAME%

del %OUTPUT_PATH%

echo %OUTPUT_PATH%
@REM go build -ldflags "-s -w -H=windowsgui" -o %OUTPUT_PATH% .\cmd\%CMD_NAME%\.
go build -o %OUTPUT_PATH% .\cmd\%CMD_NAME%\.

%UPX_PATH% %OUTPUT_PATH%
Start "" %BUILD_DIR%\out